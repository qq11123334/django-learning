from django.http import HttpResponse
from django.shortcuts import render

stock = [1232, 4233, 1412, 6666]
stock_name = {"1232" : "台積電", "4233" : "中正大學", "1412" : "宏達電", "6666" : "中正競程"}
# 送出表單
def search_form(request):
    return render(request, 'get_form.html', {"stock" : stock})

# 接收表單並回傳結果
def search(request):  
    request.encoding='utf-8'
    content = request.GET['q']
    if 'q' in request.GET and request.GET['q'] and content.isdigit() and int(content) in stock:
        response_dic = {"content" : int(content), "stock_name" : stock_name[content], "img_src" : "plot.jpg/?q=" + content}
        return render(request, 'index.html', response_dic)    
    else:
        return render(request, 'error.html', {"content" : content})
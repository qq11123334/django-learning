from django.urls import path
from django.contrib.staticfiles.views import serve
 
from . import get_form, draw
urlpatterns = [
    path('search/', get_form.search),
    path('search-form/', get_form.search_form),
    path('search/plot.jpg/', draw.get_mat),
    path('favicon.ico/', serve, {'path' : 'favicon.ico'}),
]
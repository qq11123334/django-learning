import numpy as np
import matplotlib.pyplot as plt
from django.http import HttpResponse
from django.shortcuts import render
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.dates import DateFormatter


def get_mat(request):
    date = ["11/30", "12/1", "12/2", "12/3", "12/4", "12/7", "12/8", "12/9", "12/10", "12/11"]
    price = [10, 9, 8, 10, 8, 7, 6, 13, 14, 18]

    fig = Figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.plot(date, price, '-')
    ax.set_xlabel('Date')
    ax.set_ylabel('Price')
    ax.set_title("Stock " + request.GET['q'])

    canvas = FigureCanvasAgg(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response
